package com.mitchellbosecke.benchmark;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Locale;

import org.junit.BeforeClass;
import org.junit.Test;

import com.mitchellbosecke.pebble.error.PebbleException;

import freemarker.template.TemplateException;

/**
 *
 * @author Martin Kouba
 */
public class ExpectedOutputTest {

    @BeforeClass
    public static void beforeClass() {
        Locale.setDefault(Locale.ENGLISH);
    }
    
    @Test
    public void testEnjoyOutput() throws Exception {
    		Enjoy enjoy = new Enjoy();
        enjoy.setup();
        assertOutput(enjoy.benchmark());
    }
    
    @Test
    public void testBeetlOutput() throws Exception {
		Beetl beetl = new Beetl();
        beetl.setup();
        assertOutput(beetl.benchmark());
    }

    @Test
    public void testFreemarkerOutput() throws Exception {
        Freemarker freemarker = new Freemarker();
        freemarker.setup();
        assertOutput(freemarker.benchmark());
    }
    
    @Test
    public void testRockerOutput() throws Exception {
        Rocker rocker = new Rocker();
        rocker.setup();
        assertOutput(rocker.benchmark());
    }

    @Test
    public void testPebbleOutput() throws Exception {
        Pebble pebble = new Pebble();
        pebble.setup();
        assertOutput(pebble.benchmark());
    }

    @Test
    public void testVelocityOutput() throws Exception {
        Velocity velocity = new Velocity();
        velocity.setup();
        assertOutput(velocity.benchmark());
    }

    @Test
    public void testMustacheOutput() throws Exception {
        Mustache mustache = new Mustache();
        mustache.setup();
        assertOutput(mustache.benchmark());
    }

    @Test
    public void testRythmOutput() throws Exception {
        Rythm rythm = new Rythm();
        rythm.setup();
        assertOutput(rythm.benchmark());
    }

    // @Test
    public void testThymeleafOutput() throws Exception {
        Thymeleaf thymeleaf = new Thymeleaf();
        thymeleaf.setup();
        assertOutput(thymeleaf.benchmark());
    }

    @Test
    public void testTrimouOutput() throws Exception {
        Trimou trimou = new Trimou();
        trimou.setup();
        assertOutput(trimou.benchmark());
    }

    @Test
    public void testHbsOutput() throws Exception {
        Handlebars hbs = new Handlebars();
        hbs.setup();
        assertOutput(hbs.benchmark());
    }

    private void assertOutput(final String output) throws Exception {
        assertEquals(readExpectedOutputResource(), output.replaceAll("\\s", ""));
    }

    private String readExpectedOutputResource() throws Exception {
        StringBuilder builder = new StringBuilder();
        try (BufferedReader in = new BufferedReader(new InputStreamReader(ExpectedOutputTest.class.getResourceAsStream("/expected-output.html")))) {
            for (;;) {
                String line = in.readLine();
                if (line == null) {
                  break;
                }
                builder.append(line);
            }
        }
        // Remove all whitespaces
        return builder.toString().replaceAll("\\s", "");
    }

}
