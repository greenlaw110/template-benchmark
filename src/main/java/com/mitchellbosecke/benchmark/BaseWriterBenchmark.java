package com.mitchellbosecke.benchmark;

import org.osgl.util.IO;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

public abstract class BaseWriterBenchmark extends BaseBenchmark {
    @Override
    protected final void render(OutputStream os) throws Exception {
        Writer writer = new OutputStreamWriter(os);
        render(writer);
        IO.flush(writer);
    }

    protected abstract void render(Writer writer) throws Exception;
}
