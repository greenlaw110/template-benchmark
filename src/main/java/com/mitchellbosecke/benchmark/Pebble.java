package com.mitchellbosecke.benchmark;

import com.mitchellbosecke.pebble.PebbleEngine;
import com.mitchellbosecke.pebble.error.PebbleException;
import com.mitchellbosecke.pebble.template.PebbleTemplate;
import org.openjdk.jmh.annotations.Setup;

import java.io.Writer;
import java.util.Map;

public class Pebble extends BaseWriterBenchmark {

    private Map<String, Object> context;

    private PebbleTemplate template;

    @Setup
    public void setup() throws PebbleException {
        PebbleEngine engine = new PebbleEngine.Builder().autoEscaping(false).build();
        template = engine.getTemplate("templates/stocks.pebble.html");
        this.context = getContext();
    }

    @Override
    protected void render(Writer writer) throws Exception {
        template.evaluate(writer, context);
    }

}
