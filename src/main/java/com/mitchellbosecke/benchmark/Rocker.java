package com.mitchellbosecke.benchmark;

import com.fizzed.rocker.ContentType;
import com.fizzed.rocker.RockerOutput;
import com.fizzed.rocker.RockerOutputFactory;
import com.fizzed.rocker.runtime.OutputStreamOutput;
import com.google.common.base.Charsets;
import com.mitchellbosecke.benchmark.model.Stock;
import org.openjdk.jmh.annotations.Setup;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * Benchmark for Rocker template engine by Fizzed.
 * 
 * https://github.com/fizzed/rocker
 * 
 * @author joelauer
 */
public class Rocker extends BaseBenchmark {

    private List<Stock> items;

    @Setup
    public void setup() throws IOException {
        // no config needed, replicate stocks from context
        this.items = Stock.dummyItems();
    }

    @Override
    protected void render(final OutputStream os) {
        templates.stocks.template(this.items).render((RockerOutputFactory<RockerOutput<?>>)
                (contentType, charsetName) ->
                        new OutputStreamOutput(ContentType.HTML, os, Charsets.UTF_8));
    }
}
