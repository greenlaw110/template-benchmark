package com.mitchellbosecke.benchmark;

import com.github.jknack.handlebars.Handlebars.SafeString;
import com.github.jknack.handlebars.Helper;
import com.github.jknack.handlebars.Options;
import com.github.jknack.handlebars.Template;
import com.github.jknack.handlebars.io.ClassPathTemplateLoader;
import com.mitchellbosecke.benchmark.model.Stock;
import org.openjdk.jmh.annotations.Setup;

import java.io.IOException;
import java.io.Writer;

public class Handlebars extends BaseWriterBenchmark {

  private Object context;

  private Template template;

  @Setup
  public void setup() throws IOException {
    template = new com.github.jknack.handlebars.Handlebars(new ClassPathTemplateLoader("/", ".html"))
            .registerHelper("minus", new Helper<Stock>() {
              @Override
              public CharSequence apply(final Stock stock, final Options options)
                  throws IOException {
                return stock.getChange() < 0 ? new SafeString("class=\"minus\"") : null;
              }
            }).compile("templates/stocks.hbs");
    this.context = getContext();
  }

    @Override
    protected void render(Writer writer) throws Exception {
        template.apply(context, writer);
    }

}
