package com.mitchellbosecke.benchmark;

import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.openjdk.jmh.annotations.Setup;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

public class Freemarker extends BaseWriterBenchmark {

    private Map<String, Object> context;

    private Template template;

    @Setup
    public void setup() throws IOException {
        Configuration configuration = new Configuration(Configuration.VERSION_2_3_22);
        configuration.setTemplateLoader(new ClassTemplateLoader(getClass(), "/"));
        template = configuration.getTemplate("templates/stocks.freemarker.html");
        this.context = getContext();
    }

    @Override
    protected void render(Writer writer) throws Exception {
        template.process(context, writer);
    }

}
