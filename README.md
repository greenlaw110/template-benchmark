# template-benchmark

JMH benchmark for popular Java template engines:

* [Beetl](http://ibeetl.com/)
* [Enjoy](https://www.jfinal.com/doc/6-1)
* [Freemarker](http://freemarker.org/)
* [Mustache](https://github.com/spullara/mustache.java)
* [Pebble](http://www.mitchellbosecke.com/pebble)
* [Rocker](https://github.com/fizzed/rocker)
* [Rythm](http://rythmengine.org)
* [Thymeleaf](http://www.thymeleaf.org/)
* [Trimou](http://trimou.org/)
* [Velocity](http://velocity.apache.org/)

## 1. Running the benchmark

1. Download the source code (`git clone https://gitee.com/greenlaw110/template-benchmark.git`)
2. Build it (`mvn clean package`)
3. Run the entire benchmark suite with `java -jar target/benchmarks.jar`
4. (Optional) To run specific benchmarks, specify the benchmark name, eg. `java -jar target/benchmarks.jar Mustache Velocity`

### 1.1 Generating plot

1. Run benchmark while exporting results to csv with `java -jar target/benchmarks.jar -rff results.csv -rf csv`
2. Use gnuplot to generate plot with `gnuplot benchmark.plot`. This will output `results.png`.

### 1.2 Do it together

Run `./benchmark` will get the following steps done in a batch:
1. build the project
2. run the benchmark and generate `results.css` file
3. generate plot `results.png`

## 2. Rules of Template Engine Configuration

It is imperative that each template engine is configured in way to reflect real-world usage as opposed to it's *optimal* configuration. Typically this means an out-of-the-box configuration.

To strive for a common set of features across template engines, the following configurations are expected:
* Disabling of HTML escaping
* Template loaded from classpath prior to actual benchmark

## 3. Interpreting the Results

The benchmarks measure throughput, given in "ops/time". The time unit used is seconds.
Generally, the score represents the number of templates rendered per second; the higher the score, the better.

## 4. Example Results

![Template Comparison](results.png)
